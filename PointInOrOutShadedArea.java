package com.HW_MathLibrary;

public class PointInOrOutShadedArea {
    public static String pointInOrOutArea(double x, double y) {
        if (x >= 0 && y >= 1.5 * x - 1 && y <= x ||
                x <= 0 && y >= -1.5 * x - 1 && y <= -x) {
            System.out.println("Точка с координатами х=" + x
                    + ", y=" + y + " лежит внутри заштрихованной области");
            return "1";
        } else {
            System.out.println("Точка с координатами х=" + x
                    + ", y=" + y + " лежит вне заштрихованной области");
            return "0";
        }
    }

    public static void main(String[] args) {

        System.out.println(pointInOrOutArea(2.0, 1.0));
    }
}
