package com.HW_MathLibrary;

public class HowitzerShooting {
    private static final double ACCELERATION = 9.81;

    public static double howitzerShootingGradus(int angle, int velocity) {
        System.out.println("Расстояние при угле a в градусах:");
        return ((velocity * velocity) / ACCELERATION) *
                Math.sin(Math.toRadians(2 * angle));
    }

    public static double howitzerShootingRadian(int angle, int velocity) {
        System.out.println("Расстояние при угле а в радианах:");
        return ((velocity * velocity) / ACCELERATION) *
                Math.sin(2 * angle * Math.PI / 180);
    }

    public static void main(String[] args) {
        System.out.println(howitzerShootingGradus(30, 100));
        System.out.println(howitzerShootingRadian(30, 100));
    }
}
