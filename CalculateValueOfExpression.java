package com.HW_MathLibrary;

public class CalculateValueOfExpression {

    public static double calculateValue(double x) {
        System.out.println("При заданном значении x=" + x
                + " выражение равно:");
        double cosine = Math.cos(Math.toRadians(x));
        double sinus = Math.sin(Math.toRadians(x));
        double exponent = Math.exp(x + 1);
        double absoluteValueOfNumber = cosine / (Math.exp(sinus));
        double squareRoot = Math.sqrt(exponent + 2 * Math.exp(x) + cosine);
        double logarithmNumerator = 6 * Math.log(squareRoot);
        double logarithmDenominator = Math.log(x - exponent * sinus);
        double division = logarithmNumerator / logarithmDenominator;

        return division + absoluteValueOfNumber;
    }

    public static void main(String[] args) {

        System.out.println(calculateValue(3.0));
    }
}
